	  PROGRAM OH_absorption
c
c to calculate local emission and absoption coefficients between 
c OH(v=0)-OH*(v=0), OH(v=1)-OH*(v=1), and OH(v=1)-OH*(v=0)
c Input parameters: Tg [K], [OH] in [#/m3], [OH*] in [#/m3].
c
	  parameter (i00=39,i11=37,i10=37,itr=3,isb=12,ict=20)
      IMPLICIT REAL (A-H,J-Z), integer(i)
c
c em: emission coeff. [1/s] (A in Eq. 3), 
c ab: absorption coeff.[m2/J/s] (B in Eq. 4) 
c tf: transition freq. to be converted from wavelength
c itr: =1 (0,0), =2 (1,1), =3 (1,0)
c lab: local absorption coeff., lem: local absorption coeff
c rotational levels cut off at J=30.5
c
	common/cf1/em(isb,0:i00,itr),ab(isb,0:i00,itr),tf(isb,0:i00,itr)
	common/cf2/idx(3),vj(0:1,0:ict+2,2),bf(0:1,0:ict+2,2)
	common/cf3/lab(itr,0:ict),lem(itr,0:ict)
	common/par/hp,cs,tg
	common/inp/ohd(2)
	dimension ksum(itr),esum(itr)
	DATA (idx(i),i=1,3) /i00,i11,i10/ 

c** cs: speed of light
      cs=299792458.
	hp=6.62607004e-34
	pi=4.*atan(1.)
	ep=hp*cs/(4.*pi)
	kp=hp/cs
c
	call init
c
c Tg: gas temperature, ohx: [OH] in m^-3, oha: {OH*] in m^-3 
c Local values input
c
      Tg=2000.
	ohx=1.e20
	oha=1.e14
c
	ohd(1)=ohx
	ohd(2)=oha
c
c  *** Loop start	***
c calculate Boltzmann fraction for v and J
      call boltz_frac
c calculation of epsiron in Eq. 3
	call emis
c calculation of kappa in Eq. 4
      call absp
c
c End of loop
c
c   Integration of absorption of emission
c
      do 50 it=1,itr
	ksum(it)=0.
	esum(it)=0.
50    continue
c
      do 55 it=1,itr
	do 55 ij=0,ict
	esum(it)=esum(it)+lem(it,ij)
	ksum(it)=ksum(it)+lab(it,ij)
55    continue
c
	write(*,*) 'Emission',(ep*esum(it),it=1,itr)
	write(*,*) 'Absorption',(kp*ksum(it),it=1,itr)
c
	write(*,*) 'Normal Termination'
	end
c
c
      subroutine absp
	parameter (i00=39,i11=37,i10=37,itr=3,isb=12,ict=20)
      IMPLICIT REAL (A-H,J-Z), integer(i)
c
	common/cf1/em(isb,0:i00,itr),ab(isb,0:i00,itr),tf(isb,0:i00,itr)
	common/cf2/idx(3),vj(0:1,0:ict+2,2),bf(0:1,0:ict+2,2)
	common/cf3/lab(itr,0:ict),lem(itr,0:ict)
c
	do 10 it=1,itr
	do 10 ij=0,ict
     	lab(it,ij)=0.
10    continue
c
	it=1
	iv=0
	do 20 ij=0,ict
	do 20 is=1,isb
     	lab(it,ij)=lab(it,ij)+ab(is,ij,it)*bf(iv,ij,1)*tf(is,ij,it)
20    continue 
c
      it=2
	iv=1
	do 25 ij=0,ict
	do 25 is=1,isb
     	lab(it,ij)=lab(it,ij)+ab(is,ij,it)*bf(iv,ij,1)*tf(is,ij,it)
25    continue
c
      it=3
	iv=0
	do 30 ij=0,ict
	do 30 is=1,isb
     	lab(it,ij)=lab(it,ij)+ab(is,ij,it)*bf(iv,ij,1)*tf(is,ij,it)
30    continue
c
      return
	end
ccccccccccc
ccccccccccc
      subroutine emis
	parameter (i00=39,i11=37,i10=37,itr=3,isb=12,ict=20)
      IMPLICIT REAL (A-H,J-Z), integer(i)
c
	common/cf1/em(isb,0:i00,itr),ab(isb,0:i00,itr),tf(isb,0:i00,itr)
	common/cf2/idx(3),vj(0:1,0:ict+2,2),bf(0:1,0:ict+2,2)
	common/cf3/lab(itr,0:ict),lem(itr,0:ict)
c
	do 10 it=1,itr
	do 10 ij=0,ict
     	lem(it,ij)=0.
10    continue
c
      iv=0
	it=1
	do 20 ij=2,ict
	  do 20 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)*tf(is,ij,it)**3
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-1,2)*tf(is,ij,it)**3 
	elseif(is.eq.7) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-2,2)*tf(is,ij,it)**3
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)*tf(is,ij,it)**3
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)*tf(is,ij,it)**3
	endif
20    continue
      ij=1
	  do 25 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)*tf(is,ij,it)**3
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-1,2)*tf(is,ij,it)**3 
	elseif(is.eq.7) then
        goto 25
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)*tf(is,ij,it)**3
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)*tf(is,ij,it)**3
	endif
25    continue  
c
      ij=0
	  do 30 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)*tf(is,ij,it)**3
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9.or.is.eq.7) then
	  goto 30
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)*tf(is,ij,it)**3
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)*tf(is,ij,it)**3
	endif
30    continue
cccccccc
      iv=1
	it=3
	do 40 ij=2,ict
	  do 40 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-1,2) 
	elseif(is.eq.7) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-2,2)
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)
	endif
40    continue
      ij=1
	  do 45 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-1,2) 
	elseif(is.eq.7) then
        goto 45
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)
	endif
45    continue  
c
      ij=0
	  do 50 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9.or.is.eq.7) then
	  goto 50
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)
	endif
50    continue
cccc
      iv=1
	it=2
	do 60 ij=2,ict
	  do 60 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-1,2) 
	elseif(is.eq.7) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-2,2)
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)
	endif
60    continue
      ij=1
	  do 65 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij-1,2) 
	elseif(is.eq.7) then
        goto 65
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)
	endif
65    continue  
c
      ij=0
	  do 70 is=1,isb
	if(is.eq.3.or.is.eq.4.or.is.eq.8.or.is.eq.11) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij,2)
	elseif(is.eq.1.or.is.eq.2.or.is.eq.9.or.is.eq.7) then
	  goto 70
	elseif(is.eq.5.or.is.eq.6.or.is.eq.10) then
	  lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+1,2)
	elseif(is.eq.12) then
        lem(it,ij)=lem(it,ij)+em(is,ij,it)*bf(iv,ij+2,2)
	endif
70    continue
c
      return
	end
ccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine boltz_frac
	parameter (ict=20)
      IMPLICIT REAL (A-H,J-Z), integer(i)
c 
	common/cf2/idx(3),vj(0:1,0:ict+2,2),bf(0:1,0:ict+2,2)
	common/par/hp,cs,tg
	dimension qv(0:1,2)
	common/inp/ohd(2)
c
      tw=tg*0.695
c
      qv1=1./(1.-exp(-3737.7941/tw))
	qv2=1./(1.-exp(-3178.3554/tw))
	qv(0,1)=1./qv1
	qv(1,1)=exp(-(5430.751-1861.639)/tw)
	qv(0,2)=1./qv2
	qv(1,2)=exp(-(4566.403-1578.528)/tw)
c
	do 15 i3=1,2
      do 10 iv=0,1
      sumj=0.
 	do 5 ij=0,ict+2
	jj=float(ij)+0.5
	bf(iv,ij,i3)=2.*(2.*jj+1.)*exp(-vj(iv,ij,i3)/tw)
	sumj=sumj+bf(iv,ij,i3)
5     continue
c
 	do 7 ij=0,ict+2
	bf(iv,ij,i3)=bf(iv,ij,i3)/sumj*qv(iv,i3)*ohd(i3)
7     continue
c
10    continue
15     continue
	return
	end
ccccc
ccccc
      subroutine init
	parameter (i00=39,i11=37,i10=37,itr=3,isb=12,ict=20)
      IMPLICIT REAL (A-H,J-Z), integer(i)
c
	common/cf1/em(isb,0:i00,itr),ab(isb,0:i00,itr),tf(isb,0:i00,itr)
	common/cf2/idx(3),vj(0:1,0:ict+2,2),bf(0:1,0:ict+2,2)
	common/par/hp,cs,tg
c
       open(1,file= 'line_position.txt')
	 open(2,file= 'absorption.txt')
	 open(3,file= 'emission.txt')
	 open(4,file= 'Rlevel.txt')
c
      do 10 ix=1,itr
 	do 10 ij=0,idx(ix)
	read(1,*) dummy,(tf(is,ij,ix),is=1,isb)
	read(2,*) dummy,(ab(is,ij,ix),is=1,isb)
	read(3,*) dummy,(em(is,ij,ix),is=1,isb)
10    continue
c
      do 15 ix=1,itr
 	do 15 ij=0,idx(ix)
	do 15 is=1,isb
	if(tf(is,ij,ix).eq.0.) goto 15
	tf(is,ij,ix)=cs/tf(is,ij,ix)
15    continue
c
	do 16 i3=1,2
      do 20 iv=0,1
 	do 20 ij=0,ict+2
	read(4,*) idummy,vj(iv,ij,i3)
20    continue
16     continue
c
      close(1)
	close(2)
	close(3)
	close(4)
cccccccc
	return
	end



